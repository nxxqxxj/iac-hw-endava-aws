##########  PROVIDER DECLARATION   ##########
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

##########  SOURCE AZ DECLARATION   ##########
data "aws_availability_zones" "available" {
  state = "available"
}

##########  VPC SETUP   ##########
resource "aws_vpc" "IaC-HW-VPC" {
  cidr_block = "10.10.0.0/16"
  tags       = {
    "Name"        = "IaC-HW-VPC"
    "created-by"  = "nxxqxxj"
  }
}

resource "aws_internet_gateway" "IaC-HW-VPC-IGW" {
  vpc_id = aws_vpc.IaC-HW-VPC.id
  tags   = {
    "Name"        = "IaC-HW-VPC-IGW"
    "created-by"  = "nxxqxxj"
  }
}

##########  PUBLIC SUBNET CREATION   ##########
resource "aws_subnet" "IaC-HW-VPC-SNet-AZ1" {
  vpc_id                  = aws_vpc.IaC-HW-VPC.id
  cidr_block              = "10.10.1.0/24"
  availability_zone       = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true
  tags                    = {
    "Name"        = "IaC-HW-VPC-SNet-AZ1"
    "created-by"  = "nxxqxxj"
  }
}

resource "aws_route_table" "IaC-HW-VPC-PUBLIC-RT" {
  vpc_id  = aws_vpc.IaC-HW-VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IaC-HW-VPC-IGW.id
  }
  tags    = {
    "Name"        = "IaC-HW-VPC-PUBLIC-RT"
    "created-by"  = "nxxqxxj"
  }
}

resource "aws_route_table_association" "IaC-HW-VPC-PUBLIC-RT-ASSOCIATION" {
  subnet_id      = aws_subnet.IaC-HW-VPC-SNet-AZ1.id
  route_table_id = aws_route_table.IaC-HW-VPC-PUBLIC-RT.id
}

##########  PRIVATE SUBNET CREATION   ##########
resource "aws_subnet" "IaC-HW-VPC-SNet-AZ2" {
  vpc_id            = aws_vpc.IaC-HW-VPC.id
  cidr_block        = "10.10.2.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
  tags              = {
    "Name"        = "IaC-HW-VPC-SNet-AZ2"
    "created-by"  = "nxxqxxj"
  }
}

##########  SECURITY GROUP   ##########
resource "aws_security_group" "IaC-HW-SG" {
  name   = "IaC-HW-SG"
  vpc_id = aws_vpc.IaC-HW-VPC.id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    "Name"        = "IaC-HW-SG"
    "created-by"  = "nxxqxxj"
  }
}

##########  EC2 INSTANCE SETUP   ##########
resource "tls_private_key" "SSHKey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "IaC-HW-SSHKey" {
  key_name   = "IaC-HW-SSHKey"
  public_key = tls_private_key.SSHKey.public_key_openssh
}

resource "aws_instance" "IaC-HW-EC2" {
  ami                         = "ami-0f924dc71d44d23e2"
  instance_type               = "t2.micro"
  security_groups             = ["${aws_security_group.IaC-HW-SG.id}"]
  key_name                    = aws_key_pair.IaC-HW-SSHKey.key_name
  subnet_id                   = aws_subnet.IaC-HW-VPC-SNet-AZ1.id
  user_data                   = "${file("data.sh")}"
  tags                        = {
    "Name"        = "IaC-HW-EC2"
    "created-by"  = "nxxqxxj"
  }
}
