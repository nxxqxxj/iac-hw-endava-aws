  #! /bin/bash
  sudo yum update -y
  sudo yum install -y docker
  sudo systemctl enable docker.service
  sudo systemctl start docker.service
  sudo docker pull nxxqxxj/frontendava:latest
  sudo docker run -d -p 80:3033 nxxqxxj/frontendava:latest