output "public_ip_address" {
  value = aws_instance.IaC-HW-EC2.public_ip
}

output "private_key" {
  value     = tls_private_key.SSHKey.private_key_pem
  sensitive = true
}